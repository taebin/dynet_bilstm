import dynet as dy

m = dy.ParameterCollection()
pW = m.add_parameters((8,2))
pV = m.add_parameters((1,8))
pb = m.add_parameters((8))
# #
dy.renew_cg()
#
W = dy.parameter(pW)
V = dy.parameter(pV)
b = dy.parameter(pb)

print(b.value())
x = dy.vecInput(2)
output = dy.logistic(V * (dy.tanh((W * x) + b)))
# x.set([0,0])
# print(output.value())
y = dy.scalarInput(0)
# print(y.value())
# print()
loss = dy.binary_log_loss(output, y)

def create_xor_instances(num_rounds=2000):
    questions = []
    answers = []
    for round in range(num_rounds):
        for x1 in 0, 1:
            for x2 in 0,1:
                answer = 0 if x1 == x2 else 1
                questions.append((x1,x2))
                answers.append(answer)
    return questions, answers

questions, answers = create_xor_instances()
# trainer = dy.SimpleSGDTrainer(m)

def create_xor_network(pW, pV, pb, inputs, expected_answer):
    dy.renew_cg()
    W = dy.parameter(pW)
    V = dy.parameter(pV)
    b = dy.parameter(pb)
    x = dy.vecInput(len(inputs))
    x.set(inputs)
    y = dy.scalarInput(expected_answer)
    output = dy.logistic(V*(dy.tanh((W*x)+b)))
    print(output.value())
    loss = dy.binary_log_loss(output, y)
    return loss

total_loss = 0
seen_instances = 0

m2 = dy.ParameterCollection()
pW = m2.add_parameters((8,2))
pV = m2.add_parameters((1,8))
pb = m2.add_parameters((8))
trainer = dy.SimpleSGDTrainer(m2)

for question, answer in zip(questions, answers):
    loss = create_xor_network(pW, pV, pb, question, answer)
    seen_instances += 1
    total_loss += loss.value()
    loss.backward()
    trainer.update()
    # if(seen_instances % 100 == 0):
        # print("average loss is: {}".format(total_loss/seen_instances))