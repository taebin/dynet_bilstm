originalFile = open("result_arc.txt", "r", encoding='utf-8')
testresultFile = open("result_testfile.txt", "a", encoding='utf-8')

originalSentence = originalFile.read().split('\n\n')

for sentence in originalSentence:
    # 하나라도 틀린 어절이 있으면
    answerList = []
    predictedList = []
    answerLine = sentence.split('\n')[-2][11:]
    predictedLine = sentence.split('\n')[-1][11:]
    if answerLine != predictedLine:
        for line in sentence.split('\n')[:-2]:
            testresultFile.write(line + '\n')
        for answer, predicted in zip(eval(answerLine), eval(predictedLine)):
            if answer != predicted:
                answerList.append(answer)
                predictedList.append(predicted)
        testresultFile.write('answer:    ')
        for ans in answerList:
            testresultFile.write(str(ans) + ' ')
        testresultFile.write('\n')
        testresultFile.write('predicted: ')
        for pre in predictedList:
            testresultFile.write(str(pre) + ' ')
        testresultFile.write('\n\n')
