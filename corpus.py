emb_dic = {}
empty_vec = []
with open("total_embedding_blank_skipgram_10epoch_200dim.vec", "r", encoding='utf-8') as word_embed:
    lines = word_embed.readlines()[1:]
    for line in lines:
        emb_dic[line.split(' ')[0]] = line[line.find(' ')+1:-1]

with open("morph_empty_vec_200dim.txt", "r", encoding='utf-8') as emvec:
     line = emvec.readline()
     for vec in line.split(' '):
         empty_vec.append(float(vec))

emb_dic['EMPTY'] = empty_vec * 4

def get_featvec_sum(morph_list):
    result_vec = []
    for morph in morph_list:
        vec = []
        for val in emb_dic[morph].split(' ')[:-1]:
            vec.append(float(val))
        result_vec.append(vec)

    return [sum(x) for x in zip(*result_vec)]

def get_featvec(morph_list):
    vec = []
    if len(morph_list) == 1:
        for val in emb_dic[morph_list[0]].split(' ')[:-1]:
            # print(emb_dic[morph_list[0]], val)
            vec.append(float(val))
        vec += empty_vec * 3
    elif len(morph_list) == 2:
        for val in emb_dic[morph_list[0]].split(' ')[:-1]:
            vec.append(float(val))
        vec += empty_vec * 2
        for val in emb_dic[morph_list[1]].split(' ')[:-1]:
            vec.append(float(val))
    elif len(morph_list) == 3:
        for val in emb_dic[morph_list[0]].split(' ')[:-1]:
            vec.append(float(val))
        for val in emb_dic[morph_list[1]].split(' ')[:-1]:
            vec.append(float(val))
        vec += empty_vec
        for val in emb_dic[morph_list[2]].split(' ')[:-1]:
            vec.append(float(val))
    else:
        for val in emb_dic[morph_list[0]].split(' ')[:-1]:
            vec.append(float(val))
        for val in emb_dic[morph_list[1]].split(' ')[:-1]:
            vec.append(float(val))
        for val in emb_dic[morph_list[-2]].split(' ')[:-1]:
            vec.append(float(val))
        for val in emb_dic[morph_list[-1]].split(' ')[:-1]:
            vec.append(float(val))

    return vec


class Eojul:
    def __init__(self, pos=0, eojul='', morpheme='', head_pos=-1):
        self.pos = pos
        self.eojul = eojul
        self.morpheme = morpheme
        self.head_pos = head_pos
        self.wordvec = get_featvec(self.morpheme.split(' + '))
        # self.label = label

    def __str__(self):
        return '%d\t%s\t%s\t%d' % (self.pos, self.eojul, self.morpheme, self.head_pos)


class Sentence:
    def __init__(self, sentence='', eojuls=[]):
        self.sentence = sentence
        self.eojuls = [self.convert(eojul) for eojul in eojuls]

    def convert(self, eojul):
        if type(eojul) is str:
            f = eojul.split('\t')
            return Eojul(int(f[0]), f[1], f[2], int(f[3]))
        elif isinstance(eojul, Eojul):
            return eojul

    def __getitem__(self, pos):
        return self.eojuls[pos]

    def __str__(self):
        ret = self.sentence
        ret += '\n'
        ret += '\n'.join([str(eojul) for eojul in self.eojuls])
        return ret


class Corpus:
    def __init__(self, filepath, encoding='utf-8'):
        self.corpus = []

        with open(filepath, encoding=encoding) as f:
            data = [entry for entry in f.read().split('; ')[1:]]
        for d in data:
            lines = d.split('\n')
            sentence = lines[0]
            eojuls = lines[1:-2]
            self.corpus.append(Sentence(sentence, eojuls))

    def __getitem__(self, pos):
        return self.corpus[pos]

    def __str__(self):
        return [str(sentence) for sentence in self.corpus]
