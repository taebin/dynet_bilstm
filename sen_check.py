
from corpus import *
from sen_parser import arc_hybrid_train

sentences = Corpus("train.txt")
print("corpus load complete")

for sentence in sentences:
    print(sentence)
    arc_hybrid_train(sentence)