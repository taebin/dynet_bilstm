import numpy as np
import dynet as dy

from corpus import *
from sen_parser import arc_hybrid_train
import time

# ninf = -float('inf')
epochs = 100
bilstmNum = 10   # 5, 8, 10, 12 bilstm
max_word_len = 100
init_input_dim = 800     # X[나/NP] = 200 dim
# lstm_hidden = init_input_dim * 0.5
lstm_out = 125
mlp_input_dim = bilstmNum * 2 * lstm_out     # 1000 dim
mlp_hidden = 100
mlp_out = 1     # SH, LA

# mlp_empty_vec = []
#
# with open("mlp_empty_vec.txt", "r", encoding='utf-8') as f:
#     for elem in f.readline().split(' '):
#         mlp_empty_vec.append(float(elem))

sentences = Corpus("train.txt")
print("corpus load complete")
print("10epoch 200dim concat 8 BiLSTM rectify DROP 0.5")
m = dy.ParameterCollection()
mlp_W = m.add_parameters((mlp_hidden, mlp_input_dim), dy.GlorotInitializer())   # 1000 x 100
mlp_W2 = m.add_parameters((mlp_out, mlp_hidden), dy.GlorotInitializer())
mlp_b = m.add_parameters((mlp_hidden), dy.GlorotInitializer())
mlp_b2 = m.add_parameters((1), dy.GlorotInitializer())

final_mlp_input = m.add_parameters((1, lstm_out * 2 * bilstmNum), dy.GlorotInitializer())

lookup = m.add_lookup_parameters((max_word_len + 2, init_input_dim), dy.GlorotInitializer())
trainer = dy.AdamTrainer(m)
trainer.set_sparse_updates(False)

builders = [
    dy.LSTMBuilder(1, init_input_dim, lstm_out, m),
    dy.LSTMBuilder(1, init_input_dim, lstm_out, m)
]
# builders_layer2 = [
#     dy.LSTMBuilder(1, lstm_hidden, lstm_out, m),
#     dy.LSTMBuilder(1, lstm_hidden, lstm_out, m)
# ]

def create_training_instance(sentence):
    x_input, y_output, index_num, config_num = arc_hybrid_train(sentence)

    return x_input, y_output, index_num


def create_lstm_network(inputs, y_output, index_arr, builders):
    # per sentence
    dy.renew_cg()
    # dy.renew_cg()
    seq_len = len(inputs)
    f_init, b_init = [b.initial_state() for b in builders]
    # f_init2, b_init2 = [b.initial_state() for b in builders_layer2]
    lst = emb_dic['EMPTY']
    lookup.init_row(0, lst)
    for w_index in range(1, seq_len + 1):
        lookup.init_row(w_index, inputs[w_index - 1])
    lookup.init_row(seq_len+1, lst)

    w_embs = [dy.nobackprop(lookup[i]) for i in range(seq_len + 2)]

    fw = [x.output() for x in f_init.add_inputs(w_embs)]
    bw = [x.output() for x in b_init.add_inputs(reversed(w_embs))]

    # fw2 = [x.output() for x in f_init2.add_inputs(fw)]
    # bw2 = [x.output() for x in b_init2.add_inputs(reversed(bw))]

    bi = [dy.concatenate([f, b]) for f, b in zip(fw, reversed(bw))]
    # print(bi[0].value())

    p_mlp_W = dy.parameter(mlp_W)
    p_mlp_W2 = dy.parameter(mlp_W2)
    p_mlp_b = dy.parameter(mlp_b)
    p_mlp_b2 = dy.parameter(mlp_b2)
    # final_mlp_input = dy.parameter(final_mlp)

    errs = []
    # print(y_output)
    for index_element, y in zip(index_arr, y_output):
        index_count = 0
        for index in index_element:
            if index_count == 0:
                final_mlp_input = dy.concatenate([bi[index + 1]])
                index_count += 1
            else:
                final_mlp_input = dy.concatenate([final_mlp_input, bi[index + 1]])
                index_count += 1

        # NO DROPOUT
        # y_hat = dy.logistic(p_mlp_W2 * dy.rectify((p_mlp_W * final_mlp_input) + p_mlp_b) + p_mlp_b2)
        # DROPOUT
        y_hat = dy.logistic(p_mlp_W2 * dy.dropout(dy.rectify((p_mlp_W * dy.dropout(final_mlp_input, 0.25)) + p_mlp_b), 0.25) + p_mlp_b2)

        err = dy.binary_log_loss(y_hat, dy.scalarInput(y))

        errs.append(err)

    return dy.esum(errs)


start = time.time()

for epoch in range(1, epochs + 1):
    total_sen_num = 1
    total_loss = 0
    for sentence in sentences:
        X, Y, index_arr = create_training_instance(sentence)

        loss = create_lstm_network(X, Y, index_arr, builders)
        total_loss += loss.value()
        loss.backward()
        trainer.update()
        if total_sen_num == 1 or total_sen_num % 2000 == 0 or total_sen_num == 56970:
            print("average loss after {} epoch, {} sentence is: {}".format(epoch, total_sen_num, total_loss/total_sen_num))
        total_sen_num += 1

        if total_sen_num == 56970:
            with open(str(bilstmNum) + "bilstm_" + str(mlp_hidden) + "mlphidden_" + str(int(lstm_out)) + "lstmhidden", "a", encoding='utf-8') as f:
                f.write(str(epoch) + ": " + str(total_loss/total_sen_num) + '\n')

    modelName = "after_" + str(epoch) + "_epoch_model"
    dy.save("./10epoch_200dim_concat_10bilstm_rectify_drop0.5/" + modelName, [mlp_W, mlp_W2, mlp_b, mlp_b2, final_mlp_input,
                                                                            builders[0], builders[1]
        # , builders_layer2[0], builders_layer2[1]
                                                                            ])
    print("total training time: {} seconds".format(time.time()-start))
