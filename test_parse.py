import numpy as np
from operator import itemgetter
from decimal import Decimal, ROUND_HALF_UP
import dynet as dy

from corpus import *
from sen_parser import *
import time

result_file = open("result_arc.txt", "a", encoding='utf-8')
# epochs = 100
max_word_len = 100
init_input_dim = 800     # X[나/NP] = 200 dim
# lstm_hidden = 125
# mlp_input_dim = 4 * 2 * lstm_hidden     # 1000 dim
# mlp_hidden = 100
# mlp_out = 1     # SH, LA
# mlp_empty_vec = []
epoch_num = 22

# with open("mlp_empty_vec.txt", "r", encoding='utf-8') as f:
#     for elem in f.readline().split(' '):
#         mlp_empty_vec.append(float(elem))

file_name = "./10epoch_200dim_concat_10bilstm_rectify_drop0.5/after_" + str(epoch_num) + "_epoch_model"
sentences = Corpus("test.txt")
print("corpus load complete")
m = dy.ParameterCollection()
mlp_W, mlp_W2, mlp_b, mlp_b2, final_mlp, builders_a, builders_b = dy.load(file_name, m)
print("model loaded")
lookup = m.add_lookup_parameters((max_word_len + 2, init_input_dim))

builders = [
    builders_a,
    builders_b
]

# builders_2 = [
#     builders_l2_a,
#     builders_l2_b
# ]

def parse_test_sent(sentence):

    total_model_arcset = []

    inputs, buffer, stack, arcset, correct_arcset = arc_hybrid_test(sentence)

    dy.renew_cg()
    # dy.renew_cg()
    seq_len = len(inputs)
    f_init, b_init = [b.initial_state() for b in builders]
    # f_init2, b_init2 = [b.initial_state() for b in builders_2]
    # print(np.array(inputs[-1]))

    lst = emb_dic['EMPTY']
    lookup.init_row(0, lst)
    for w_index in range(1, seq_len + 1):
        lookup.init_row(w_index, inputs[w_index - 1])
    lookup.init_row(seq_len+1, lst)

    w_embs = [dy.nobackprop(lookup[i]) for i in range(seq_len + 2)]

    fw = [x.output() for x in f_init.add_inputs(w_embs)]
    bw = [x.output() for x in b_init.add_inputs(reversed(w_embs))]

    # fw2 = [x.output() for x in f_init2.add_inputs(fw)]
    # bw2 = [x.output() for x in b_init2.add_inputs(reversed(bw))]

    bi = [dy.concatenate([f, b]) for f, b in zip(fw, reversed(bw))]

    p_mlp_W = dy.parameter(mlp_W)
    p_mlp_W2 = dy.parameter(mlp_W2)
    p_mlp_b = dy.parameter(mlp_b)
    p_mlp_b2 = dy.parameter(mlp_b2)


    while not (len(buffer) == 1 and len(stack) == 0):
        # idx_lst = []
        if len(buffer) == 0:
            break
        if len(stack) == 0:
            stack.append(buffer[0])
            buffer.pop(0)
        elif len(stack) == 1:
            print("---------------stack length 1--------------")
            if len(buffer) > 1:
                idx_lst = getDependents(total_model_arcset, buffer[0].pos, buffer[1].pos, stack[-1].pos)
            else:
                idx_lst = getDependents(total_model_arcset, buffer[0].pos, -1, stack[-1].pos)
            concat_lst = [bi[x + 1] for x in idx_lst]
            final_mlp_input = dy.concatenate(concat_lst)
            y = dy.logistic(p_mlp_W2 * dy.rectify(p_mlp_W * final_mlp_input + p_mlp_b) + p_mlp_b2)

            if y.scalar_value() >= 0.5:
                # LEFT
                print("LEFT")

                # print(stack[0].eojul, buffer[0].eojul)
                print('[ ', end='')
                for i in range(len(stack)):
                    print(stack[i].eojul + " ", end='')
                print('] [ ', end='')
                for i in range(len(buffer)):
                    print(buffer[i].eojul + " ", end='')
                print(']')

                print(y.scalar_value())
                total_model_arcset.append([stack[-1].pos, buffer[0].pos])
                stack.pop(-1)
            else:
                print("SHIFT")

                print('[ ', end='')
                for i in range(len(stack)):
                    print(stack[i].eojul + " ", end='')
                print('] [ ', end='')
                for i in range(len(buffer)):
                    print(buffer[i].eojul + " ", end='')
                print(']')

                print(y.scalar_value())
                stack.append(buffer[0])
                buffer.pop(0)

        elif len(stack) == 2:
            print("---------------stack length 2--------------")
            if len(buffer) > 1:
                idx_lst = getDependents(total_model_arcset, buffer[0].pos, buffer[1].pos, stack[-1].pos, stack[-2].pos)
            else:
                idx_lst = getDependents(total_model_arcset, buffer[0].pos, -1, stack[-1].pos, stack[-2].pos)
            concat_lst = [bi[x + 1] for x in idx_lst]
            final_mlp_input = dy.concatenate(concat_lst)
            y = dy.logistic(p_mlp_W2 * dy.rectify(p_mlp_W * final_mlp_input + p_mlp_b) + p_mlp_b2)

            if y.scalar_value() >= 0.5:
                # LEFT
                print("LEFT")
                # print(stack[0].eojul, buffer[0].eojul)
                print('[ ', end='')
                for i in range(len(stack)):
                    print(stack[i].eojul + " ", end='')
                print('] [ ', end='')
                for i in range(len(buffer)):
                    print(buffer[i].eojul + " ", end='')
                print(']')

                print(y.scalar_value())
                total_model_arcset.append([stack[-1].pos, buffer[0].pos])
                stack.pop(-1)
            else:
                print("SHIFT")

                print('[ ', end='')
                for i in range(len(stack)):
                    print(stack[i].eojul + " ", end='')
                print('] [ ', end='')
                for i in range(len(buffer)):
                    print(buffer[i].eojul + " ", end='')
                print(']')

                print(y.scalar_value())
                stack.append(buffer[0])
                buffer.pop(0)
        else:
            print("---------------stack length 3 over --------------")
            if len(buffer) > 1:
                idx_lst = getDependents(total_model_arcset, buffer[0].pos, buffer[1].pos, stack[-1].pos, stack[-2].pos, stack[-3].pos)
            else:
                idx_lst = getDependents(total_model_arcset, buffer[0].pos, -1, stack[-1].pos, stack[-2].pos, stack[-3].pos)
            concat_lst = [bi[x + 1] for x in idx_lst]
            final_mlp_input = dy.concatenate(concat_lst)
            y = dy.logistic(p_mlp_W2 * dy.rectify(p_mlp_W * final_mlp_input + p_mlp_b) + p_mlp_b2)

            if y.scalar_value() >= 0.5:
                print("LEFT")
                # print(stack[0].eojul, buffer[0].eojul)
                print('[ ', end='')
                for i in range(len(stack)):
                    print(stack[i].eojul + " ", end='')
                print('] [ ', end='')
                for i in range(len(buffer)):
                    print(buffer[i].eojul + " ", end='')
                print(']')

                print(y.scalar_value())
                total_model_arcset.append([stack[-1].pos, buffer[0].pos])
                stack.pop(-1)
            else:
                print("SHIFT")

                print('[ ', end='')
                for i in range(len(stack)):
                    print(stack[i].eojul + " ", end='')
                print('] [ ', end='')
                for i in range(len(buffer)):
                    print(buffer[i].eojul + " ", end='')
                print(']')

                print(y.scalar_value())
                stack.append(buffer[0])
                buffer.pop(0)


    # return stack, buffer, arcset


    return sorted(total_model_arcset, key=itemgetter(0)), correct_arcset



cnt = 0

total_correct_arc_num = 0
correct_num = 0
sen_num = 0

start = time.time()
for sentence in sentences:
    sen_num += 1
    output, answer = parse_test_sent(sentence)
    result_file.write(str(sentence) + '\n')
    result_file.write("answer:    " + str(answer) + '\n')
    result_file.write("predicted: " + str(output) + '\n\n')
    total_correct_arc_num += (len(answer) + 1)

    for out in output:
        if out in answer:
            correct_num += 1
    correct_num += 1

t2 = time.time()
value = Decimal((correct_num / total_correct_arc_num) * 100)

print("-----------------with epoch " + str(epoch_num) + " model----------------------")
print("total sentence number: {}".format(sen_num))
print("total_answer_arc_num: {}".format(total_correct_arc_num))
print("total_correct_arc_num: {}".format(correct_num))

print("parsing time: {} seconds".format(t2-start))
print("total accuracy: {}".format(Decimal(value.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))))
print("----------------------------------------------------------")

print("\n10epoch 200dim VECTOR SUM sigmoid dropout 0.5")




