import dynet as dy

# z1 = dy.inputTensor([1,2, 3,4])
# print(z1.value())

pc = dy.ParameterCollection()
NUM_LAYERS = 2
INPUT_DIM = 50
HIDDEN_DIM = 10
builder = dy.LSTMBuilder(NUM_LAYERS, INPUT_DIM, HIDDEN_DIM, pc)
test_lst = [x for x in range(50)]
s0 = builder.initial_state()
x1 = dy.inputTensor(test_lst)
s1 = s0.add_input(x1)
y1 = s1.output()
print(s1.output())
print(y1.value())
print(s1.h())
print(s1.s())

s2 = s1.add_input(x1)
y2 = s2.output()
print(y2.value())
print(s2.h())
print(s2.s())

s3 = s2.add_input(x1)
y3 = s3.output()
print(y3.value())
print(s3.h())
print(s3.s())

s4 = s3.add_input(x1)
y4 = s4.output()
print(y4.value())
print(s4.h())
print(s4.s())

s5 = s4.add_input(x1)
y5 = s5.output()
print(y5.value())
print(s5.h())
print(s5.s())

s6 = s5.add_input(x1)
y6 = s6.output()[0]
print(y6.value())
print(s6.h())
print(s6.s())

assert s5.prev() == s3
assert s4.prev() == s3