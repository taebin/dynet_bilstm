def getStacktopLmostDep(word_idx, arcset):
    lmost_idx = 200
    nodep_flag = 0
    if word_idx == -1:
        lmost_idx = -1
    else:
        for arc in arcset:
            if arc[-1] != word_idx:
                continue
            else:
                if arc[0] < lmost_idx:
                    lmost_idx = arc[0]
                    nodep_flag = 1

    return -1 if nodep_flag == 0 else lmost_idx - 1


def getStacktopRmostDep(word_idx, arcset):
    rmost_idx = 0
    nodep_flag = 0
    if word_idx == -1:
        rmost_idx = -1
    else:
        for arc in arcset:
            if arc[-1] != word_idx:
                continue
            else:
                if arc[0] > rmost_idx:
                    rmost_idx = arc[0]
                    nodep_flag = 1
    return -1 if nodep_flag == 0 else rmost_idx - 1


def getBuffertopLmostDep(word_idx, arcset):
    btlmost_idx = 200
    nodep_flag = 0
    if word_idx == -1:
        btlmost_idx = -1
    else:
        for arc in arcset:
            if arc[-1] != word_idx:
                continue
            else:
                if arc[0] < btlmost_idx:
                    btlmost_idx = arc[0]
                    nodep_flag = 1
    return -1 if nodep_flag == 0 else btlmost_idx - 1


def getStackSecondtopLmostDep(word_idx, arcset):
    lmost_idx = 200
    nodep_flag = 0
    if word_idx == -1:
        lmost_idx = -1
    else:
        for arc in arcset:
            if arc[-1] != word_idx:
                continue
            else:
                if arc[0] < lmost_idx:
                    lmost_idx = arc[0]
                    nodep_flag = 1

    return -1 if nodep_flag == 0 else lmost_idx - 1
def getStackSecondtopRmostDep(word_idx, arcset):
    rmost_idx = 0
    nodep_flag = 0
    if word_idx == -1:
        rmost_idx = -1
    else:
        for arc in arcset:
            if arc[-1] != word_idx:
                continue
            else:
                if arc[0] > rmost_idx:
                    rmost_idx = arc[0]
                    nodep_flag = 1
    return -1 if nodep_flag == 0 else rmost_idx - 1
def getStackThirdtopLmostDep(word_idx, arcset):
    lmost_idx = 200
    nodep_flag = 0
    if word_idx == -1:
        lmost_idx = -1
    else:
        for arc in arcset:
            if arc[-1] != word_idx:
                continue
            else:
                if arc[0] < lmost_idx:
                    lmost_idx = arc[0]
                    nodep_flag = 1

    return -1 if nodep_flag == 0 else lmost_idx - 1
def getStackThirdtopRmostDep(word_idx, arcset):
    rmost_idx = 0
    nodep_flag = 0
    if word_idx == -1:
        rmost_idx = -1
    else:
        for arc in arcset:
            if arc[-1] != word_idx:
                continue
            else:
                if arc[0] > rmost_idx:
                    rmost_idx = arc[0]
                    nodep_flag = 1
    return -1 if nodep_flag == 0 else rmost_idx - 1

def getDependents(arcset, buffer_idx, buffer2_idx = -1, stack1_idx = -1, stack2_idx = -1, stack3_idx = -1):


    STL = getStacktopLmostDep(stack1_idx, arcset)
    STR = getStacktopRmostDep(stack1_idx, arcset)
    BTL = getBuffertopLmostDep(buffer_idx, arcset)  # 8 BiLSTM

    SSTL = getStackSecondtopLmostDep(stack2_idx, arcset)
    SSTR = getStackSecondtopRmostDep(stack2_idx, arcset)    # 10 BiLSTM
    SSSTL = getStackThirdtopLmostDep(stack3_idx, arcset)
    SSSTR = getStackThirdtopRmostDep(stack3_idx, arcset)    # 12 BiLSTM

    return [stack3_idx if stack3_idx == -1 else stack3_idx - 1, #SSSTL, SSSTR,
            stack2_idx if stack2_idx == -1 else stack2_idx - 1, SSTL, SSTR,
            stack1_idx if stack1_idx == -1 else stack1_idx - 1, STL, STR, BTL,
            buffer_idx - 1,
            buffer2_idx if buffer2_idx == -1 else buffer2_idx - 1]


def arc_hybrid_train(sentence):

    bu = []     # buffer
    st = []     # stack
    ar = []     # arcset

    X = []
    Y = []
    index_number = []   # [[-1, -1, 2, 3], [1, 2, 4, 6], ... ]

    for eojul in sentence:
        bu.append(eojul)
        X.append(eojul.wordvec)

    sequence_length = len(bu)
    config_num = 0
    while not (len(bu) == 1 and len(st) == 0):
        # index_number:
        if len(st) == 0:
            if len(bu) > 1:
                input_deps = getDependents(ar, bu[0].pos, bu[1].pos)
                index_number.append(input_deps)
            else:
                input_deps = getDependents(ar, bu[0].pos)
                index_number.append(input_deps)
            st.append(bu[0])
            bu.pop(0)
            Y.append(0)     # SHIFT
        elif len(st) == 1:
            if len(bu) > 1:
                input_deps = getDependents(ar, bu[0].pos, bu[1].pos, st[-1].pos)
                index_number.append(input_deps)
            else:
                input_deps = getDependents(ar, bu[0].pos, -1, st[-1].pos)
                index_number.append(input_deps)
            if st[-1].head_pos == bu[0].pos:    # LEFT
                ar.append([st[-1].pos, bu[0].pos])
                Y.append(1)    # LEFT-ARC
                st.pop(-1)
            else:               # SHIFT
                Y.append(0)
                st.append(bu[0])
                bu.pop(0)
        elif len(st) == 2:
            if len(bu) > 1:
                input_deps = getDependents(ar, bu[0].pos, bu[1].pos, st[-1].pos, st[-2].pos)
                index_number.append(input_deps)
            else:
                input_deps = getDependents(ar, bu[0].pos, -1, st[-1].pos, st[-2].pos)
                index_number.append(input_deps)
            if st[-1].head_pos == bu[0].pos:
                ar.append([st[-1].pos, bu[0].pos])
                Y.append(1)  # LEFT-ARC
                st.pop(-1)
            else:
                Y.append(0)
                st.append(bu[0])
                bu.pop(0)
        else:
            if len(bu) > 1:
                input_deps = getDependents(ar, bu[0].pos, bu[1].pos, st[-1].pos, st[-2].pos, st[-3].pos)
                index_number.append(input_deps)
            else:
                input_deps = getDependents(ar, bu[0].pos, -1, st[-1].pos, st[-2].pos, st[-3].pos)
                index_number.append(input_deps)
            if st[-1].head_pos == bu[0].pos:
                ar.append([st[-1].pos, bu[0].pos])
                Y.append(1)  # LEFT-ARC
                st.pop(-1)
            else:
                Y.append(0)
                st.append(bu[0])
                bu.pop(0)
        config_num += 1
    assert len(X) == sequence_length
    assert len(index_number) == config_num == len(Y)

    return X, Y, index_number, config_num


def arc_hybrid_test(sentence):
    bu = []     # buffer
    st = []     # stack
    ar = []     # arcset
    correct_ar = []
    X = []

    for eojul in sentence:
        if eojul.head_pos != 0:
            correct_ar.append([eojul.pos, eojul.head_pos])

    for eojul in sentence:
        bu.append(eojul)
        X.append(eojul.wordvec)

    return X, bu, st, ar, correct_ar